//imports the SPI library (needed to communicate with Gamebuino's screen)
#include <SPI.h>
#include <EEPROM.h>
//imports the Gamebuino library


//#define DEBUG

#define CENTER_X 42
#define CENTER_Y 24

#define SMALL_HEX_PERCENT 0.12



#define SIDES 6

#include <Gamebuino.h>

void updateHexagon();

const byte microHex[] PROGMEM = {32,14,0xDB,0x3D,0xF3,0xC0,0xFB,0x7D,0xF7,0xC0,0xFB,0x61,0x36,0xC0,0xDB,0x61,0xF6,0xC0,0xDB,0x61,0xE6,0xC0,0xDB,0x61,0x76,0xC0,0xDB,0x7D,0x37,0xC0,0xDB,0x39,0x37,0xC0,0x0,0x0,0x0,0x0,0xAD,0x5D,0xDD,0x80,0xA9,0x55,0x15,0x40,0xEC,0x9D,0x55,0x40,0xA9,0x55,0x55,0x40,0xAD,0x55,0xDD,0x40,};
const byte hscr[] PROGMEM = {16,4,0xAE,0xD8,0xE8,0x94,0xA2,0x98,0xAE,0xD4,};
const byte gmover[] PROGMEM = {24,15,0xFB,0xDE,0xF8,0xFB,0x5E,0xC0,0xC3,0x5A,0xC0,0xDB,0x5A,0xF8,0xCB,0xDA,0xC0,0xFB,0x5A,0xF8,0xFB,0x5A,0xF8,0x0,0x0,0x0,0xFB,0x5E,0xF0,0xCB,0x58,0xC8,0xCB,0x58,0xC8,0xCB,0x5E,0xF0,0xCB,0x58,0xC8,0xF9,0x9E,0xC8,0xF9,0x9E,0xC8,};
//hexagon points
float hexagon[10][2];

float angle = 0;

void (*gmStates[4]) ();

enum GameStates{
  MENU = 0,
  GAME = 1,
  GAMEOVER = 2
};

#define HSC_OFFSET 2
void writeHSC(char name[4], int score,char pos);
unsigned int readHSCScore(char pos);
void readHSCName(char* buffer,char pos);
void initHighScore();

float pump;
float score;
float gamespeed = 1;
float gamespeed_incr = 0.0005;
#define GAMESPEED_CAP 1.5f

int lastRotation;
#define CHANGE_ROT_THRESHOLD 80
#define CHANGE_ROT_ODD 10
#define SPEED_INCREMENT 0.01
#define SPEED_CAP 0.08
#define INIT_ROT_SPEED 0.03
float rot_speed =0.2;

int pump_delay;
#define MAX_PUMP 1.3
#define PUMP_SPEED 0.04
#define PUMP_DELAY 16
#define FRAMES_PER_SEC 20.0
#define SEC_PER_FRAMES (1/FRAMES_PER_SEC)

GameStates GameState;

#include "walls.h"
#include "player.h"
//creates a Gamebuino object named gb
Gamebuino gb;

float hg_score = 0.0;
int gameOverTimer = 0;
void initGameOver(){
  GameState = GAMEOVER;
  gameOverTimer = 0;
  hg_score = ((float)readHSCScore(0))/10;
  if (score > hg_score)
    writeHSC("AAA",score*10,0);
}

void initMenu(){
  GameState = MENU;
  rot_speed = INIT_ROT_SPEED;
  pump = 2;
  hg_score = ((float)readHSCScore(0))/10;
}

void absoluteControls(){
}

void updateGameOver(){
  //do stuff
  updateHexagon();
  gameControls();

  drawGameOver();
  gameOverTimer ++;
  if (gameOverTimer > 10)
    #define MAX_PUMP_OVER 2.5
    if (pump < MAX_PUMP_OVER)
      pump += 0.1;
    else
      pump = MAX_PUMP_OVER;
}

void drawGameOver(){

  drawHexagon();
  drawWalls(gb, hexagon, 6, angle);
  drawPlayer(gb,angle);

  if (gameOverTimer < 6 && gameOverTimer%3 < 2){
    gb.display.fillScreen(INVERT);
  }

  if (pump == MAX_PUMP_OVER){
    gb.display.setColor(WHITE);
    gb.display.fillRect(31,2,21,15);
    gb.display.setColor(BLACK);

    gb.display.drawBitmap(31,2,gmover);

    gb.display.setColor(BLACK,WHITE);

    gb.display.cursorX = 66;
    gb.display.cursorY = 6;
    gb.display.print(F("TIME"));

    char b[4];
    sprintf(b,"%03d",(int) floor(score));
/*    if (b[0]=='0')
     b[0] = ' ';
    if (b[1]=='0')
      b[1] = ' ';*/

    gb.display.setColor(BLACK,WHITE);
    gb.display.drawPixel(77,13);
    gb.display.drawPixel(77,15);

    


    gb.display.cursorY = 12;
    gb.display.cursorX = 65;
    gb.display.print(b);
    gb.display.cursorX = 79;
    gb.display.print((int) ((score-floor(score))*10));

    gb.display.cursorX = 66;
    gb.display.cursorY = 18;
    gb.display.print(F("BEST"));

    sprintf(b,"%03d",(int) floor(hg_score));
    gb.display.drawPixel(77,25);
    gb.display.drawPixel(77,27);

    gb.display.cursorY = 24;
    gb.display.cursorX = 65;
    gb.display.print(b);
    gb.display.cursorX = 79;
    gb.display.print((int) ((hg_score-floor(hg_score))*10));


    gb.display.cursorX = 27;
    gb.display.cursorY = 33;

    gb.display.print(F("\26Restart"));

    gb.display.cursorX = 27;
    gb.display.cursorY = 40;

    gb.display.print(F("\27Menu"));
  }


}
void menuControls(){
  if(gb.buttons.pressed(BTN_A))
  {
    playInit();
  }
  if(gb.buttons.pressed(BTN_C))
    gb.changeGame();
}

void gameControls(){
  if(gb.buttons.pressed(BTN_C))
    initMenu();
  if(gb.buttons.pressed(BTN_B))
      playInit();
}

void updatePump(){
  //pump
    if (pump>1)
      pump -= PUMP_SPEED;
    else
      pump = 1;
    pump_delay ++;
    if(pump == 1 && pump_delay > PUMP_DELAY)
    {
      pump = MAX_PUMP;
      pump_delay == 0;
    }
}

void drawHexagon(){
  for(byte i = 0; i < 6; i ++){
      float s1x = hexagon[i][0] * SMALL_HEX_PERCENT * pump;
      float s1y = hexagon[i][1] * SMALL_HEX_PERCENT * pump;
      
      float s2x = hexagon[(i < 5 ? i+1 : 0)][0] * SMALL_HEX_PERCENT * pump;
      float s2y = hexagon[(i < 5 ? i+1 : 0)][1] * SMALL_HEX_PERCENT * pump;
      
      gb.display.drawLine(CENTER_X + s1x,CENTER_Y + s1y,CENTER_X + s2x, CENTER_Y + s2y);
      gb.display.setColor(GRAY);
      gb.display.drawLine(CENTER_X + s1x,CENTER_Y + s1y,CENTER_X + hexagon[i][0], CENTER_Y + hexagon[i][1]);
      gb.display.setColor(BLACK);
    }
}

void updateSpeed(){
  gamespeed += gamespeed_incr;
  if (gamespeed > (GAMESPEED_CAP))
  {
    gamespeed = GAMESPEED_CAP;
  }
}

void mainGame(){
  
      if(GameState != GAMEOVER){
        updateSpeed();
        updatePump();
        updateHexagon();
        updateRoationSpeed();
        updateWalls();
        updatePlayer();
        controls();

        score += SEC_PER_FRAMES;
      }
     
    
      //draw the small hexagon
    drawHexagon();
    
    drawWalls(gb, hexagon, 6, angle);
    drawPlayer(gb,angle);

    #ifdef DEBUG
      gb.display.setColor(BLACK,WHITE);
      gb.display.print(F("CPU:"));
      gb.display.print((int) gb.getCpuLoad());
      gb.display.setColor(BLACK);
      /*gb.display.print(F(" L:"));
      gb.display.print(getLane(SIDES));
      gb.display.print(player_angle);*/
    #endif

    drawHud();

    gameControls();
}



void drawMenu(){
  gb.display.setColor(WHITE);
  gb.display.fillRect(29,13,26,5);
  gb.display.setColor(BLACK,WHITE);
  gb.display.drawBitmap(29,4,microHex);

  gb.display.cursorX = 32;
  gb.display.cursorY = 34;
  gb.display.print(F("\25PLAY"));

  gb.display.cursorX = 32;
  gb.display.cursorY = 41;
  gb.display.print(F("\27QUIT"));

  gb.display.drawBitmap(62,36,hscr);

  char b[3];
  
  sprintf(b,"%03d",(int) floor(hg_score));
  if (b[0]=='0'){
    b[0] = ' ';
  if (b[1]=='0')
    b[1] = ' ';
  }

  gb.display.drawPixel(74,42);
  gb.display.drawPixel(74,44);

  gb.display.cursorY = 41;
  gb.display.cursorX = 62;
  gb.display.print(b);
  gb.display.cursorX = 76;
  gb.display.print((int) ((hg_score-floor(hg_score))*10));

}


void menuLoop(){
  menuControls();
  pump = 2;
  angle +=rot_speed;
    if (angle>=2 * PI) 
    {
     angle -=2*PI; 
    }
  computeHexagon();
  drawHexagon();
  drawMenu();
}


void gameInit(){
  gmStates[0] = menuLoop;
  gmStates[1] = mainGame;
  gmStates[2] = updateGameOver;
  initHighScore();
}

void playInit(){

  initWalls();
  
  gb.battery.show = false;
  lastRotation = 0;
  score = 0;
  player_angle = 0;
  pump_delay = 20;
  pump = 2;
  GameState = GAME;
  rot_speed = INIT_ROT_SPEED;
  gamespeed = 1;
}



// the setup routine runs once when Gamebuino starts up
void setup(){
  // initialize the Gamebuino object
  Serial.begin(9600);
  gb.begin();

  gb.setFrameRate(FRAMES_PER_SEC);
  //gb.titleScreen(F("microHex"));
  
  gb.battery.show = false;
  gameInit();
  initMenu();
  //gb.popup(F("Let's go!"), 100);
}






void loop(){
    if(gb.update())
    {
      absoluteControls();
      (*gmStates[GameState]) ();
    }
    
  }

void computeHexagon(){
  //compute the points
    float c_angle = cos(PI * 2 / 6);
    float s_angle = sin(PI * 2 / 6);
    
    hexagon[0][0] = LINE_LENGTH * cos(angle);
    hexagon[0][1] = LINE_LENGTH * sin(angle);
      
    for(byte i = 1; i < 6; i++)
    {
      hexagon[i][0] = hexagon[i-1][0]*c_angle - hexagon[i-1][1]*s_angle;
      hexagon[i][1] = hexagon[i-1][0]*s_angle + hexagon[i-1][1]*c_angle;
    }
}


void updateRoationSpeed(){
  lastRotation ++;
    if(lastRotation > CHANGE_ROT_THRESHOLD && random(0,CHANGE_ROT_ODD+1) == CHANGE_ROT_ODD && abs(rot_speed <SPEED_CAP))
    {
      lastRotation = 0;
      rot_speed = (random(0,2) == 0 ? 1:-1) * (abs(rot_speed) + SPEED_INCREMENT);
    }
}

void updateHexagon(){
    angle +=rot_speed;
    if (angle>=2 * PI) 
    {
     angle -=2*PI; 
    }

    //speed rotation
    

    
    computeHexagon(); 

    
}

void drawHud(){
  char b[10];
  gb.display.setColor(BLACK);
  
  
  sprintf(b,"%03d",(int) floor(score));
  int start = 62;
  if (b[0]=='0'){
    b[0] = ' ';
    start += 4; 
  if (b[1]=='0'){
    b[1] = ' ';
    start += 4;
  }
    
  }

  for(char i = 0; i < 7; i ++){
    gb.display.drawFastHLine(start+i,i,22-i);
  }

  gb.display.setColor(WHITE);
  gb.display.drawPixel(79,1);
  gb.display.drawPixel(79,3);

  gb.display.cursorY = 0;
  gb.display.cursorX = 67;
  gb.display.print(b);
  gb.display.cursorX = 81;
  gb.display.print((int) ((score-floor(score))*10));
}




#define GAME_ID 0x02FF
char isValidHSC(){
  int valid = ((((int)EEPROM.read(0)) << 8) + (int)EEPROM.read(1));
  return valid == GAME_ID;
}

void initHighScore(){
  if (!isValidHSC()){
    EEPROM.write(0,GAME_ID>>8);
    EEPROM.write(1,GAME_ID);
    writeHSC("AAA",00000,0);
  }
}

void writeHSC(char name[4], int score,char pos){
  pos *= 5;
  EEPROM.write(pos+HSC_OFFSET,name[0]);
  EEPROM.write(pos+1+HSC_OFFSET,name[1]);
  EEPROM.write(pos+2+HSC_OFFSET,name[2]);
  EEPROM.write(pos+3+HSC_OFFSET,score>>8);
  EEPROM.write(pos+4+HSC_OFFSET,score);
}

unsigned int readHSCScore(char pos){
  pos *= 5;
  return ((((int)EEPROM.read(pos+3+HSC_OFFSET)) << 8) + (int)EEPROM.read(pos+4+HSC_OFFSET));
}
