#ifndef _PLAYER_
#define _PLAYER_

#include <Gamebuino.h>
#include "walls.h"

#define PLAYER_DISTANCE_PERCENT 0.16
#define PLAYER_DEFAULT_DISTANCE (LINE_LENGTH * PLAYER_DISTANCE_PERCENT)

void updatePlayer();
void controls(Gamebuino gb);
void drawPlayer(Gamebuino gb, float angle);
byte getLane(byte laneCount);
extern GameStates GameState;
float player_angle;
float player_speed = 0.35;


#endif _PLAYER_