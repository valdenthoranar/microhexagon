


void controls(){
	if (GameState != GAMEOVER)
	{

		if (gb.buttons.repeat(BTN_A,0)){
    player_angle += player_speed ;
    while (player_angle > 2*PI)
        player_angle -= 2 * PI; 
    byte lane = getLane(6);
    if(checkCollision(lane,PLAYER_DISTANCE_PERCENT))
    	player_angle = ((PI * 2 / 6) * lane)-0.05;
  }
  if (gb.buttons.repeat(BTN_DOWN,0)){
    player_angle -= player_speed ;
    while (player_angle < 0)
        player_angle += 2 * PI; 
    byte lane = getLane(6);
    if(checkCollision(lane,PLAYER_DISTANCE_PERCENT)){
    	player_angle = (PI * 2 / 6) * ((lane+1)%SIDES)+0.05;
    }
    
  }
  while (player_angle > 2*PI)
    	player_angle -= 2 * PI; 
  while (player_angle < 0)
    player_angle += 2 * PI; 
	}
  
}


void updatePlayer(){
	if (checkCollision(getLane(6),PLAYER_DISTANCE_PERCENT))
		initGameOver();

}

byte getLane(byte laneCount){
		return ((char) (player_angle/(PI * 2 / laneCount)))%laneCount;
}

void drawPlayer(Gamebuino gb, float angle){
	//draw the player
    char x = (cos(player_angle + angle) * PLAYER_DEFAULT_DISTANCE * pump + CENTER_X)  ;
    char y = (sin(player_angle + angle) * PLAYER_DEFAULT_DISTANCE * pump + CENTER_Y)  ;

	  
    gb.display.drawPixel(x+1,y);
    gb.display.drawPixel(x-1,y);
    gb.display.drawPixel(x,y+1);
    gb.display.drawPixel(x,y-1);
    gb.display.setColor(WHITE);
    gb.display.drawPixel(x,y);
    gb.display.drawPixel(x+2,y);
    gb.display.drawPixel(x+1,y+1);
    gb.display.drawPixel(x+1,y-1);
    gb.display.drawPixel(x,y+2);
    gb.display.drawPixel(x,y-2);
    gb.display.drawPixel(x-1,y-1);
    gb.display.drawPixel(x-1,y+1);
    gb.display.drawPixel(x-2,y);
}