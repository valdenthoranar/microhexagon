#ifndef _WALLS_
#define _WALLS_
#include <Gamebuino.h>

#define PATTERN_NUMBER 4
#define MAXWALLS 15
#define COLLISION_CORRECTION (0.05)
#define NULL_LANE -1
#define LINE_LENGTH 50

#define DRAW_MODE 1 //0 wireframe, 1 black (traingle)

typedef struct{
  char lane;
  float distance;
  float width;
} wall;

struct _pattern{
	byte wall_num;
	float length;
	float distance;
	char rotation;
	bool mirror;
	wall *walls;
} ;
typedef struct _pattern _patern;
_pattern patterns[PATTERN_NUMBER];

#include "wallsData.h"

// update all the walls
void updateWalls();
//draw all the walls
void drawWalls(Gamebuino gb, float hexagonData[][2], byte gonSides, const float angle);
//check if there is a collision on the lane given at the given distance
bool checkCollision(const byte lane, const float distance);
//init all walls
void initWalls();

void addPatern(byte index);

float wallSpeed = 0.015;

void storePattern(_pattern origin, _pattern *dest);

#endif _WALLS_