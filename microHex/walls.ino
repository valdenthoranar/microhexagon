#include "walls.h"

void initWalls(){
  for (byte i = 0; i < PATTERN_NUMBER; i++)
  {
    patterns[i].distance = 0;
    patterns[i].length = 0;
    patterns[i].wall_num = 0;
    patterns[i].rotation = 0;
    patterns[i].walls = NULL;
    patterns[i].mirror = 0;
  }
  /*walls[0].lane = 0;
  walls[0].distance = 0.4;
  walls[0].width = 2;*/  //random(0,PATTERN_LIST_NUMBER)
   storePattern(patternList[PATTERN_LIST_NUMBER-1],&patterns[0]);
  #ifdef DEBUG
   
  #else
   // storePattern(patternList[random(0,PATTERN_LIST_NUMBER)],&patterns[0]);
  #endif
  patterns[0].mirror = false;
}

void addPatern(byte index){
  float min_pos = 1;
  for(byte i = 0; i < PATTERN_NUMBER; i++)
    if (min_pos < patterns[i].distance + patterns[i].length)
      min_pos = patterns[i].distance + patterns[i].length;
  storePattern(patternList[random(0,PATTERN_LIST_NUMBER)], &(patterns[index]));
  patterns[index].distance = min_pos;
  patterns[index].rotation = random(0,SIDES);
  patterns[index].mirror = random(0,2);
}

void updateWalls(){
	for(byte i = 0; i < PATTERN_NUMBER; i ++){
		if(patterns[i].distance + patterns[i].length > 0){
      patterns[i].distance -= wallSpeed * gamespeed;
    }
    else
    {
      Serial.print(F("ADDING PATTERN \n"));
      addPatern(i);
    }
	}
}

void storePattern(struct _pattern *origin, struct _pattern *dest){
  dest->wall_num = pgm_read_byte_near(&(origin->wall_num));
  dest->distance = pgm_read_float_near(&(origin->distance));
  dest->length = pgm_read_float_near(&(origin->length));
  dest->walls = (wall*) pgm_read_word_near(&(origin->walls));
}

bool checkCollision(const byte lane, const float distance){
	for(byte j = 0; j < PATTERN_NUMBER; j++){
    for(byte i = 0; i < patterns[j].wall_num; i ++){
      char wall_lane = (pgm_read_byte_near(&(patterns[j].walls[i].lane)) + patterns[j].rotation)%SIDES;
      if (patterns[j].mirror == true)
        wall_lane = SIDES - 1 - wall_lane;
      if ( wall_lane == lane){
        if (distance + COLLISION_CORRECTION > patterns[j].distance + pgm_read_float_near(&(patterns[j].walls[i].distance)) && distance + COLLISION_CORRECTION < patterns[j].distance + pgm_read_float_near(&(patterns[j].walls[i].distance)) + pgm_read_float_near(&(patterns[j].walls[i].width)) )
          return true;
    }
  }
  }

  
	return false;

};

void drawWalls(Gamebuino gb, float hexagonData[][2], byte gonSides, const float angle){
	//draw walls
  for(byte j = 0; j < PATTERN_NUMBER; j++)
  {
    if (patterns[j].distance + patterns[j].length > SMALL_HEX_PERCENT){
      for(byte i = 0; i < patterns[j].wall_num; i ++){
            float wall_dist = pgm_read_float_near(&(patterns[j].walls[i].distance));
            float wall_width = pgm_read_float_near(&(patterns[j].walls[i].width));
            byte wall_lane = pgm_read_byte_near(&(patterns[j].walls[i].lane));
                if(wall_lane != NULL_LANE && (patterns[j].distance + wall_dist)*pump < 1 && patterns[j].distance + wall_dist + wall_width > SMALL_HEX_PERCENT)
                  {
                    wall_lane = (wall_lane + patterns[j].rotation) % SIDES;
                    if (patterns[j].mirror == true)
                      wall_lane = SIDES - 1 - wall_lane;
                    float width =  (patterns[j].distance + wall_dist + wall_width)*pump < 1 ? (patterns[j].distance + wall_dist + wall_width)*pump : 1; 
                    gb.display.setColor(BLACK);

                    float s1x = hexagonData[wall_lane][0];
                    float s1y = hexagonData[wall_lane][1];
                
                    float s2x = hexagonData[(wall_lane < 5 ? wall_lane+1 : 0)][0];
                    float s2y = hexagonData[(wall_lane < 5 ? wall_lane+1 : 0)][1];
                    /*           s1x_______s2x       
                                  \      . /
                                   \  .   /
                                    \____/
                                  s3x   s4x
                    */
                    float distance = patterns[j].distance + wall_dist > SMALL_HEX_PERCENT ? patterns[j].distance + wall_dist : SMALL_HEX_PERCENT;
                   float s3x = s1x * distance * pump;
                    float s3y = s1y * distance * pump;
                    float s4x = s2x * distance * pump;
                    float s4y = s2y * distance * pump;

                    s1x *= width;
                    s1y *= width;
                
                    s2x *= width;
                    s2y *= width;

                    #if (DRAW_MODE == 0)
                    // Wireframe method => FAST
                    gb.display.drawLine(s1x + CENTER_X, s1y + CENTER_Y,
                                            s2x + CENTER_X, s2y + CENTER_Y);
                    gb.display.drawLine(s1x + CENTER_X, s1y + CENTER_Y,
                                            s3x + CENTER_X, s3y + CENTER_Y);

                    gb.display.drawLine(s3x + CENTER_X, s3y + CENTER_Y,
                                            s4x + CENTER_X, s4y + CENTER_Y);
                    gb.display.drawLine(s2x + CENTER_X, s2y + CENTER_Y,
                                            s4x + CENTER_X, s4y + CENTER_Y);
                    #else
                    // FILL THE HEXAGON => VERY SLOW
                    gb.display.fillTriangle(((int)s1x) + CENTER_X, ((int)s1y) + CENTER_Y,
                                            ((int)s2x) + CENTER_X, ((int)s2y) + CENTER_Y,
                                            ((int)s3x) + CENTER_X, ((int)s3y) + CENTER_Y);
                    gb.display.fillTriangle(((int)s4x) + CENTER_X, ((int)s4y) + CENTER_Y,
                                            ((int)s2x) + CENTER_X, ((int)s2y) + CENTER_Y,
                                            ((int)s3x) + CENTER_X, ((int)s3y) + CENTER_Y);

                    /*for(float j = walls[i].distance; j < width; j += 0.05)
                    {
                      gb.display.drawLine(s1x*j + CENTER_X, s1y*j + CENTER_Y,
                                            s2x*j + CENTER_X, s2y*j + CENTER_Y);
                    }
                    gb.display.drawLine(s1x*width + CENTER_X, s1y*width + CENTER_Y,
                                            s2x*width + CENTER_X, s2y*width + CENTER_Y);*/
                    #endif

                  }
              }
    }
    
  }
    
}